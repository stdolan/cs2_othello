#include "board.h"

/*
 * Make a standard 8x8 othello board and initialize it to the standard setup.
 */
Board::Board() {
    taken.set(3 + 8 * 3);
    taken.set(3 + 8 * 4);
    taken.set(4 + 8 * 3);
    taken.set(4 + 8 * 4);
    black.set(4 + 8 * 3);
    black.set(3 + 8 * 4);
}

/*
 * Destructor for the board.
 */
Board::~Board() {
}

/*
 * Returns a copy of this board.
 */
Board *Board::copy() {
    Board *newBoard = new Board();
    newBoard->black = black;
    newBoard->taken = taken;
    return newBoard;
}

bool Board::occupied(int x, int y) {
    return taken[x + 8*y];
}

bool Board::get(Side side, int x, int y) {
    return occupied(x, y) && (black[x + 8*y] == (side == BLACK));
}

void Board::set(Side side, int x, int y) {
    taken.set(x + 8*y);
    black.set(x + 8*y, side == BLACK);
}

bool Board::onBoard(int x, int y) {
    return(0 <= x && x < 8 && 0 <= y && y < 8);
}

 
/*
 * Returns true if the game is finished; false otherwise. The game is finished 
 * if neither side has a legal move.
 */
bool Board::isDone() {
    return !(hasMoves(BLACK) || hasMoves(WHITE));
}

/*
 * Returns true if there are legal moves for the given side.
 */
bool Board::hasMoves(Side side) {
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move move(i, j);
            if (checkMove(&move, side)) return true;
        }
    }
    return false;
}

/*
 * Returns true if a move is legal for the given side; false otherwise.
 */
bool Board::checkMove(Move *m, Side side) {
    // Passing is only legal if you have no moves.
    if (m == NULL) return !hasMoves(side);

    int X = m->getX();
    int Y = m->getY();

    // Make sure the square hasn't already been taken.
    if (occupied(X, Y)) return false;

    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            // Is there a capture in that direction?
            int x = X + dx;
            int y = Y + dy;
            if (onBoard(x, y) && get(other, x, y)) {
                do {
                    x += dx;
                    y += dy;
                } while (onBoard(x, y) && get(other, x, y));

                if (onBoard(x, y) && get(side, x, y)) return true;
            }
        }
    }
    return false;
}

/*
 * Modifies the board to reflect the specified move.
 */
void Board::doMove(Move *m, Side side) {
    // A NULL move means pass.
    if (m == NULL) return;

    // Ignore if move is invalid.
    if (!checkMove(m, side)) return;

    int X = m->getX();
    int Y = m->getY();
    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            int x = X;
            int y = Y;
            do {
                x += dx;
                y += dy;
            } while (onBoard(x, y) && get(other, x, y));

            if (onBoard(x, y) && get(side, x, y)) {
                x = X;
                y = Y;
                x += dx;
                y += dy;
                while (onBoard(x, y) && get(other, x, y)) {
                    set(side, x, y);
                    x += dx;
                    y += dy;
                }
            }
        }
    }
    set(side, X, Y);
}

/*
 * Current count of given side's stones.
 */
int Board::count(Side side) {
    return (side == BLACK) ? countBlack() : countWhite();
}

/*
 * Current count of black stones.
 */
int Board::countBlack() {
    return black.count();
}

/*
 * Current count of white stones.
 */
int Board::countWhite() {
    return taken.count() - black.count();
}

/*
 * Sets the board state given an 8x8 char array where 'w' indicates a white
 * piece and 'b' indicates a black piece. Mainly for testing purposes.
 */
void Board::setBoard(char data[]) {
    taken.reset();
    black.reset();
    for (int i = 0; i < 64; i++) {
        if (data[i] == 'b') {
            taken.set(i);
            black.set(i);
        } if (data[i] == 'w') {
            taken.set(i);
        }
    }
}

/**
 *  Returns the score of a board for a given side, heuristically.
 */
int Board::score(Side s)
{
	int modifier = 0;
	for (int x = 0; x < 8; x++)
	{
		for (int y = 0; y < 8; y++)
		{	
			// Corners of board
			if ((x == 0  && y == 0)
			    || (x == 7  && y == 7)
			    || (x == 0  && y == 7)
			    || (x == 7  && y == 0))
			{
				modifier += 3;
			}
			
			// On sides of board
			if ((x == 2  && y == 0)
			    || (x == 3  && y == 0)
			    || (x == 4  && y == 0)
			    || (x == 5  && y == 0)
			    || (x == 0  && y == 2)
			    || (x == 0  && y == 3)
			    || (x == 0  && y == 4)
			    || (x == 0  && y == 5)
			    || (x == 7  && y == 2)
			    || (x == 7  && y == 3)
			    || (x == 7  && y == 4)
			    || (x == 7  && y == 5)
			    || (x == 2  && y == 7)
			    || (x == 3  && y == 7)
			    || (x == 4  && y == 7)
			    || (x == 5  && y == 7))
			{
				modifier += 2;
			}
			
			// Spaces adjacent to corners
			if ((x == 1 && y == 0)
			    || (x == 0  && y == 1)
			    || (x == 6  && y == 0)
			    || (x == 7  && y == 1)
			    || (x == 1  && y == 7)
			    || (x == 0  && y == 6)
			    || (x == 6  && y == 7)
			    || (x == 7  && y == 6))
			{
				modifier += -1;
			}
			
			// Spaces diagonal to corners
			if ((x == 1 && y == 1)
			    || (x == 6  && y == 6)
			    || (x == 1  && y == 6)
			    || (x == 6  && y == 1))
			{
				modifier += -2;
			}
		}
	}
	
	return this->count(s) - this->count((Side)((int)s ^ 1)) + modifier;
}

/**
 * Returns the move that results in the greatest minimum possible score
 * after ply more moves.
 * 
 * Currently ply is unimplemented, defaults to 2.
 */

int Board::minmax(Move *move, Side playerSide, int ply)
{
	Board nextBoard = *this;
	nextBoard.doMove(move, playerSide);
	Side opponentSide = (Side)((int)playerSide ^ 1);
	vector<Board> allBoards;
	allBoards.push_back(nextBoard);
	int minScore = 100;
	ply -= 1;
	while (ply != 0)
	{
		Side s = (ply % 2) ? playerSide : opponentSide;
		int origVectorSize = allBoards.size();
		for (int k = 0; k < origVectorSize; k++)
		{
			Board current = *allBoards.begin();
			allBoards.erase(allBoards.begin());
			if (current.hasMoves(s))
			{
				Move m(0, 0);
				for (int i = 0; i < 8; i++)
				{
					m.setX(i);
					for (int j = 0; j < 8; j++)
					{
						m.setY(j);
						if (current.checkMove(&m, s))
						{
							Board next = current;
							next.doMove(&m, s);
							allBoards.push_back(next);
						}
					}
				}
			}
		}
		ply -= 1;
	}
	for (vector<Board>::iterator it = allBoards.begin(); it != allBoards.end(); ++it)
	{
		int boardScore = it->score(playerSide);
		if (boardScore < minScore)
		{
			minScore = boardScore;
		}
	}
	return minScore;
}
