#include "player.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
	
	playerSide = side;
	opponentSide = (Side)((int)side ^ 1);
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentMove, int msLeft) {
	if (opponentMove != NULL)
	{
		board.doMove(opponentMove, opponentSide);
	}
	
    if (board.hasMoves(playerSide))
    {
		// Impossible value for 8x8 Othello
		int nextScore = -100, possibleScore;
		Move possible(0, 0), *next = new Move(0, 0);
		
		for (int i = 0; i < 8; i++)
		{
			possible.setX(i);
	        for (int j = 0; j < 8; j++)
	        {
	            possible.setY(j);
				if (board.checkMove(&possible, playerSide))
				{
					int ply;
					if (testingMinimax)
					{
						ply = 2;
					}
					else
					{
						ply = (msLeft == -1) ? 8 : 6;
					}
					possibleScore = board.minmax(&possible, playerSide, ply);
					if (possibleScore > nextScore)
					{
						*next = possible;
						nextScore = possibleScore;
					}
				}
			}
		}
		board.doMove(next, playerSide);
		return next;
	}

    return NULL;
}
