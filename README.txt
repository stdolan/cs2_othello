The additions to my AI that make it tournament worthy are the 6-ply
search depth (that, given the 16 minutes in the tournament, should
be fine), and the fact that it uses minmax, as a number of people
haven't implemented it, so BogoPlayer should do okay. It also uses a
heuristic that takes into account where each piece is in relation to
the edges and corners, so that will help.
